<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
    <form action="workshop1_view.php" method = "POST" id="frmForm">
    * First name : <input type="text" id="firstname" name="firstname">
    * Last name : <input type="text" id ="lastname" name="lastname">
    <button type ="submit" >View</button>
    </form>
 
<div></div>

<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

<script>
    $(function () {
        $('form').submit(function (e) { 
            e.preventDefault(); //เป็นคำสั่งให้ disable สิ่งที่เราจะทำก่อน
            if(!Validation()){
                return false;
            }
         
         $.ajax({
             type: "GET",
             url: $(this).attr('action'),
             data: $(this).serialize(),  // รวบค่าทั้งหมดที่อยู่ใน form เพื่อส่งค่าแบบ asynchonous
             //dataType: "dataType",
             success: function (response) {   // อันนี้คือการ callback
               $('div').html(response);
               $('form').hide();
              // alert(response)  ;
             }
         });
        });
    });

    function Validation(){
        
        var v_msg ="";
          var v_firstname =  $('#firstname')  ; //document.getElementBuID
          var v_lastname =  $('#lastname')  ;

          if($.trim(v_firstname.val()) == ""){
             // alert('Please in put Firstanme');
              v_msg = v_msg +'first_name';
              //v_firstname.focus();
             // return false;
          }
          if($.trim(v_lastname.val()) == ""){
             // alert('Please in put Lastname');
              v_msg = v_msg +'last_name';
             // v_lastname.focus();
             // return false;
          }

          if(v_msg.length>0){
              alert("Please input" + v_msg);
              return false;
          }
          return true;
    }
</script>
</body>
</html>